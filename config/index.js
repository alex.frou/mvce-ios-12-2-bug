'use strict'

// see http://vuejs-templates.github.io/webpack for documentation.
const path = require('path')
//dynamic env_dev
let env_dev = require('./dev.env')
if (JSON.stringify(process.env.FIRE_ENV) !== env_dev.FIRE_ENV) {
  console.log('Force firebase dev.env to ' + process.env.FIRE_ENV)
  env_dev.FIRE_ENV = JSON.stringify(process.env.FIRE_ENV)
}

//dynamic env_build
let env_prod = require('./prod.env')
if (JSON.stringify(process.env.FIRE_ENV) !== env_prod.FIRE_ENV) {
  console.log('Force firebase prod.env to ' + process.env.FIRE_ENV)
  env_prod.FIRE_ENV = JSON.stringify(process.env.FIRE_ENV)
}

module.exports = {
  build: {
    env: env_prod,
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: env_dev,
    port: 8080,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}
