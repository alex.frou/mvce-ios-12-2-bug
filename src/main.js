import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router/index.js'
import Vuefire from 'vuefire'
import '@firebase/auth'
import '@firebase/firestore'
import configStore from './store'
/* import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations' */

Vue.config.productionTip = false

Vue.use(Vuefire)
Vue.use(Vuex)

Vue.config.devtools = true

const store = new Vuex.Store(configStore)

// let app

// firebase.auth().onAuthStateChanged(function (user) {
if (!window.App) {
  window.App = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
  Vue.config.devtools = true
  // debuger sentry
  /* Sentry.init({
    dsn: 'sentry id',
    integrations: [
      new Integrations.Vue({
        Vue,
        attachProps: true
      })
    ]
  }) */
}
// })
