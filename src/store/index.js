// vue import
import 'es6-promise/auto'

// firebase import
import configdb from '../firebase/firestore-config'
import firebase from '@firebase/app'
import '@firebase/firestore'

// modules import
import reservations from './modules/reservations'

// init firebase connection
firebase.initializeApp(configdb)
const db = firebase.firestore()
const settings = {}
db.settings(settings)
db.enablePersistence()

// for current test simulate uid
const uid = 'debuguserid'

/**
 * States
 */
// store main db collection in rootState
const state = {
  restaurantRef: db.collection('Restaurants')
}

/**
 * Getters
 */
const getters = {
  getRestauRef: function (state, getters) {
    return state.restaurantRef.doc(uid)
  },
  getResasRef: function (state, getters) {
    return getters['getRestauRef'].collection('reservations')
  }
}

/**
 * Mutations
 */
const mutations = {}

/**
 * Actions
 */
const actions = {
  loadStoreModules: function ({ state, rootState, commit, dispatch }) {
    console.log('LoadStores modules')
    dispatch('reservations/syncCurrentResas')
  }
}

// Export store with module
export default {
  state,
  getters,
  mutations,
  actions,
  modules: {
    reservations
  }
}
