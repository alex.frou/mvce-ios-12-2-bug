/*******************************
 * Module reservations
 *******************************/

/*************************************
 State
 **/
const state = {
  // all resa display for current service and day
  currentResas: [],
  // selected service
  selectedService: 'morning',
  // store snapshot of DB for killing when choose an other service
  unsubscribeResaSnapShot: null,
  currentResaLoading: false
}

/*************************************
 Getters
 **/
const getters = {
  getCurrentResas: function (state) {
    return state.currentResas
  },
  getCurrentResasLoading: function (state) {
    return state.currentResaLoading
  },
  getSelectedService: function (state) {
    return state.selectedService
  }
}

/*************************************
 Mutations
 **/
const mutations = {
  setCurrentResas (state, dbData) {
    state.currentResas = dbData
  },
  setCurrentResasLoading (state, bool) {
    state.currentResaLoading = bool
  },
  setSelectedService (state, service) {
    console.log('setService', service)
    state.selectedService = service
  }
}

/*************************************
 Action
 **/
const actions = {
  killSnapShotsCurrentResa: function ({ state }) {
    if (state.unsubscribeResaSnapShot !== null) {
      // console.log('kill snapshot synced resa')
      state.unsubscribeResaSnapShot()
    }
  },
  syncCurrentResas: function ({ state, rootState, commit, dispatch, getters, rootGetters }) {
    // clean last snapshot listener
    dispatch('killSnapShotsCurrentResa')
    // start loading
    commit('setCurrentResasLoading', true)
    console.log('syncCurrentResa')
    state.unsubscribeResaSnapShot = rootGetters['getResasRef'].doc('day1')
      .collection('services').doc(getters['getSelectedService'])
      .collection('resaIDs').onSnapshot(function (querySnapshot) {
        console.log('snapshot resa', querySnapshot)
        let resas = []
        if (!querySnapshot.empty) {
          querySnapshot.forEach(function (doc) {
            let resaData = doc.data()
            resas.push(resaData)
          })
        }
        console.table(resas)
        // update datas
        commit('setCurrentResas', resas)
        // data store stop loading
        commit('setCurrentResasLoading', false)
      }, function (error) {
        commit('setCurrentResasLoading', false)
        console.log('ERROR - sync current resa : ' + error)
      })
  }
}

/*************************************
 Helpers
 **/

/*************************************
 Export module
 **/
export default {
  namespaced: true, state, getters, mutations, actions
}
