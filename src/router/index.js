import Vue from 'vue'
import Router from 'vue-router'

// page component
import Resto from '@/components/Resto'

Vue.use(Router)

// define routes and simulate a login
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/restaurant',
      name: 'Restaurant',
      component: Resto,
      meta: {
        requiresAuth: true
      },
      props: true
    }
  ]
})

export default router
